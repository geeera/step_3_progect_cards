import createElement  from './CreateElement.js'
export  class TextArea extends createElement {
    render() {
        const textarea = this.create();
        this.elem = textarea;
        return textarea;
    }
}
