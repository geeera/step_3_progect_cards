import CreateElement  from './CreateElement.js';
export class Select extends CreateElement {

    render() {
        const select = this.create();
        this.elem = select;
        if (this.option) {
            for (const [key, value] of Object.entries(this.option)) {
                this.setOptions(value,key)
            }
        }
        return select
    }

    setOptions(text, value){
        const option = new Option(text,value)
        this.elem.add(option)
    }
}
