import CreateElement from '../components/CreateElement.js'
export default class Visit {
    constructor(props) {
        this.props = {...props}
    }

    div = {
        tag: 'div',
        attr: {
            className: 'card',
            id: '',
            draggable: true,
            getId(id) {
                this.id = id
            }
        }
    }

    createCardContainer(id) {
        this.div.attr.getId(id)
        const div = new CreateElement(this.div)
        return div.create()
    }
}
