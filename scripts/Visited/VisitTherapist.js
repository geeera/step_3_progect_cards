import Visit from "./Visit.js";
import CreateElement from "../components/CreateElement.js";
import {stateTherapist} from '../State/state.js'
import {CardButton} from "../Button/indexReimportBtn.js";
import {dropId} from '../State/dragDropId.js'
export class VisitTherapist extends Visit{
    drag = (e) => {
        e.dataTransfer.setData("id", this.props.id);
        setTimeout(() => {
            e.target.classList.add('hide')
        }, 0)
    }

    dragEnd = (e) => {
        e.target.classList.remove('hide')
    }

    dragEnter = (e) => {
        if (e.target.id) {
            dropId.id = e.target.id
        }
    }

    render() {
        const res = this.changeState(this.props, stateTherapist)

        const {
            titleCard,
            ageCard,
            blockCard,
            doctorCard,
            purposeCard,
            priorityCard,
            descriptionCard
        } = res;

        const card = this.createCardContainer(this.props.id)
        card.addEventListener('dragstart', this.drag)
        card.addEventListener('dragenter', this.dragEnter)
        card.addEventListener('dragend', this.dragEnd)
        const cardTitle = new CreateElement(titleCard).create()
        const cardAge = new CreateElement(ageCard).create()
        const cardBlock = new CreateElement(blockCard).create()
        const cardPriority = new CreateElement(priorityCard).create()
        const cardDoctor = new CreateElement(doctorCard).create()
        const cardPurpose = new CreateElement(purposeCard).create()
        const cardDescription = new CreateElement(descriptionCard).create()
        const cardBtn = new CardButton().render()

        cardBlock.append(cardPurpose, cardPriority)
        card.append(cardTitle, cardAge, cardDoctor, cardBlock, cardDescription, cardBtn)
        return card
    }

    changeState(props, state) {
        const {
            titleCard,
            ageCard,
            purposeCard,
            priorityCard,
            descriptionCard
        } = state;
        const {
            age,
            "full-name": name,
            "select-priority": priority,
            target,
            textarea
        } = props

        titleCard.content = titleCard.getContent(name)
        ageCard.content = ageCard.getContent(age)
        purposeCard.content = purposeCard.getContent(target)
        priorityCard.content = priorityCard.getContent(priority)
        descriptionCard.content = descriptionCard.getContent(textarea)

        return state
    }
}
