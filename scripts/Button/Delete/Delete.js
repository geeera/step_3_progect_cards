import Button from "../Button.js";
import {deleteCard as delCard} from "../../functions/indexFunctionReimport.js"
import {createElement} from "../../components/component.js";
import {noItems} from "../../State/NoItems/NoItems.js"

export class Delete extends Button{
    createDeleteButton() {
        const deleteBtn = this.render()
        this.elem = deleteBtn
        deleteBtn.addEventListener('click', this.deleteCard)
        return deleteBtn
    }
    deleteCard = () => {
        const cardLength = document.querySelectorAll('.card').length
        console.log(cardLength)
        if (cardLength === 1) {
            const container = document.getElementById('cards')
            const h1 = new createElement(noItems).create();
            container.after(h1)
        }
        const id = this.elem.closest('.card').id;
        delCard(id).then((res) => {
            const elem = document.getElementById(`${id}`)
            elem.remove()
        })
    }
}
