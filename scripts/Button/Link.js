import createElement from "../components/CreateElement.js";
import {ModalLogin, ModalAddVisit} from "../Modal/indexModalReimport.js";

export class Link extends createElement{
    render() {
        const link = this.create()
        this.link = link
        link.addEventListener('click', this.changeHandler)
        return link
    }

    changeHandler = () => {
        if (this.link.textContent === 'Войти') {
            const modal = new ModalLogin()
            document.body.append(modal.getData())

        } else {
            const modal = new ModalAddVisit()
            document.body.append(modal.renderModal())
        }
    }
}
