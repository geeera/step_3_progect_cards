import {req} from "../config/axios.js";

export async function deleteCard(cardId) {
    return  await req({
        url: `/cards/${cardId}`,
        method: 'DELETE'
    })
}



