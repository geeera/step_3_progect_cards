import {req} from '../config/axios.js'

export async function createCard(data) {
    return await req({
        url: '/cards',
        method: 'POST',
        data
    })
}

