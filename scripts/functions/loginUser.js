import {req} from "../config/axios.js";

export async function loginUser(data) {
    return await req({
        url: '/login',
        method: 'POST',
        data
    })
}
