import {req} from "../config/axios.js";

export async function editCard(cardID, body) {
    return req({
        url: `/cards/${cardID}`,
        method: 'PUT',
        body
    })
}
