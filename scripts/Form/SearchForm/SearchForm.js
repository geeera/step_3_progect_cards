import {SearchFormField} from "../../State/Form/Form.js"
import {Input, createElement} from "../../components/component.js"
import {SelectTagPriority, SelectPrioritySearch} from '../../State/state.js'
import {SearchInput} from "../../State/InputFilds/universalFilds.js"
import {Select} from "../../components/Select.js";
import {noItems} from "../../State/NoItems/NoItems.js";


export class SearchForm {
    renderForm(){
        const Searchform = new createElement(SearchFormField).create()
        const searchInput = new Input(SearchInput).render()
        this.elem = searchInput
        const selectPriority = new Select(SelectTagPriority, SelectPrioritySearch).render()
        searchInput.addEventListener('keyup', this.handleKeyUp)
        selectPriority.addEventListener('change',this.handleChange)

        Searchform.append(searchInput,selectPriority)
        
        return Searchform
    }

    handleKeyUp = () => {
        const form = this.elem.closest('.search-form');
        const input = form.querySelector('.search-input').value

    
        const body = document.querySelectorAll('.card h3')
        const filterarr = [...body]
        const res = filterarr.filter((item) => {
            const div = item.closest('.card')
            div.classList.remove("hide")
            return !item.textContent.includes(input) 
        })

        const cards = document.querySelectorAll('.card').length
        const noItem = document.getElementById('no-item')
        if (res.length === cards) {
            const h1 = new createElement(noItems).create();
            const container = document.getElementById('cards')
            container.append(h1)
        } else if(noItem) {
            noItem.remove()
        }

        res.forEach( el => {
            const div = el.closest('.card')
            div.classList.add("hide")
        })
    }

    handleChange = () => {
        const form = this.elem.closest('.search-form');
        const preority = form.querySelector('select').value


        const body = document.querySelectorAll('.card .card-priority')
        const filterarr = [...body]
        const res = filterarr.filter((item) => {
            const selectedValue = item.textContent.split(': ').splice(-1, 1).join("")
            const div = item.closest('.card')
            div.classList.remove("hide")
            return selectedValue !== preority && preority !== "default"
        })

        const cards = document.querySelectorAll('.card').length
        const noItem = document.getElementById('no-item')
        if (res.length === cards) {
            const h1 = new createElement(noItems).create();
            const container = document.getElementById('cards')
            container.append(h1)
        } else if(noItem) {
            noItem.remove()
        }
        res.forEach(el => {
            const div = el.closest('.card')
            div.classList.add("hide")
        })
    }
}

