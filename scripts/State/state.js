export {
    TextArea,
    Target,
    FullName,
    Age,
    SelectPriority,
    ButtonCreate,
    SelectPrioritySearch
} from './InputFilds/universalFilds.js'

export {
    Pressure,
    IndexWeight,
    DiseasesOfCardiovascularSystem
} from './InputFilds/Cardio/cardioFilds.js'

export {
    SelectTagPriority,
    SelectTag,
    InputTag,
    TextAreaTag,
    ButtonTag,
    LinkTag,
    Option
} from './TagsObjects/Tags.js'

export {
    stateCardio,
    stateDentist,
    stateTherapist
} from './CardFields/Cards.js'

export {LastDayVisited} from './InputFilds/Dentist/dentistFilds.js'
export {Form} from './Form/Form.js'
