export const SelectTag = {
    tag: 'select',
    attr: {
        className: 'select-doctor',
        name: 'select-doctor'
    }
}

export const SelectTagPriority = {
    tag: 'select',
    attr: {
        className: 'select-priority',
        name: 'select-priority'
    }
}

export const TextAreaTag = {
    tag: 'textarea',
    attr: {
        maxlength: 400,
        name: "comments",
        placeholder: "Введите коментарий",
        className: "comments-area",
        id: "area"
    }
}
export const InputTag = {
    tag: 'input',
    attr: {
        type: "text",
        className: "form-control",
        name: "visit-aim",
        placeholder: "Введите цель визита",
        required: true,
    },
    errorText: "Поле обязательное для заполнения"
}

export const ButtonTag = {
    tag: 'button',
    attr: {
        className: 'btn',
    },
    content: 'Создать'
}

export const LinkTag = {
    tag: 'a',
    attr: {
        className: 'navbar-link',
        id: 'nav-link'
    },
    content: '',
    getContent() {
        if (!!localStorage.getItem('token')) {
            this.content = 'Создать карточку'
            return this.content
        }
        this.content = 'Войти'
        return this.content
    }
}

export const Option = {
    default: 'Выберите врача',
    cardiologist: 'Кардиолог',
    dentist: 'Стоматолог',
    therapist: 'Терапевт'
}
