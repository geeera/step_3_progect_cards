export const InputEmailTag = {
    tag: 'input',
    attr: {
        type: "email",
        className: "form-control",
        id: "register-email",
        name: "register-email",
        placeholder: "Введите ваш email ",
        required: true,
    },
    errorText: "Поле обязательное для заполнения"
}

export const InputPasswordTag = {
    tag: 'input',
    attr: {
        type: "password",
        className: "form-control",
        id: "register-password",
        name: "register-password",
        placeholder: "Введите ваш пароль ",
        required: true,
    },
    errorText: "Поле обязательное для заполнения"
}

export const InputSubmitTag = {
    tag: 'input',
    attr: {
        type: "submit",
        className: "form-control btn-create",
        id: "register-submit",
        value: "Зарегистрироватся",
    }
}

