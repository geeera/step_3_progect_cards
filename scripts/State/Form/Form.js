export const Form = {
    tag: 'form',
    attr: {
        className: 'register-form',
    }
}

export const SearchFormField ={
    tag: 'form',
    attr: {
        className: 'search-form'
    }
}
