export const stateCardio = {
    titleCard: {
        tag: 'h3',
        attr: {
            className: 'card-fio'
        },
        content: '',
        getContent(contentText =  'Inna Torokina') {
            this.content = contentText
            return this.content
        }
    },
    ageCard: {
        tag: 'span',
        attr: {
            className: 'card-age'
        },
        content: '',
        getContent(contentText =  'Возраст: 18') {
            this.content = 'Возраст: ' + contentText
            return this.content
        }
    },
    doctorCard: {
        tag: 'span',
        attr: {
            className: 'card-doctor'
        },
        content: 'Врач: Кардиолог',
        getContent(contentText =  'Врач: Кардиолог') {
            this.content = 'Врач:' + contentText
            return this.content
        }
    },
    blockCard: {
        tag: 'div',
        attr: {
            className: 'card-block',
        }
    },
    purposeCard: {
        tag: 'span',
        attr: {
            className: 'card-purpose'
        },
        content: '',
        getContent(contentText =  'Цель визита: ') {
            this.content = 'Цель визита: ' + contentText
            return this.content
        }
    },
    pressureCard: {
        tag: 'span',
        attr: {
            className: 'card-pressure'
        },
        content: '',
        getContent(contentText =  'Обычное давление: ') {
            this.content = 'Обычное давление: ' + contentText
            return this.content
        }
    },
    indexWeightCard: {
        tag: 'span',
        attr: {
            className: 'card-index-weight'
        },
        content: '',
        getContent(contentText =  'Индекс массы тела: ') {
            this.content = 'Индекс массы тела: ' + contentText
            return this.content
        }
    },
    pastIllnessesCard: {
        tag: 'span',
        attr: {
            className: 'card-past-illnesses'
        },
        content: '',
        getContent(contentText =  'Перенесенные заболевания сердечно-сосудистой системы: ') {
            this.content = 'Перенесенные заболевания сердечно-сосудистой системы: ' + contentText
            return this.content
        }
    },
    illnessesCard: {
        tag: 'span',
        attr: {
            className: 'card-illnesses'
        },
        content: '',
        getContent(contentText =  ' - ') {
            this.content = ' - ' + contentText
            return this.content
        }
    },
    priorityCard: {
        tag: 'span',
        attr: {
            className: 'card-priority'
        },
        content: '',
        getContent(contentText =  'Приоритетность: ') {
            this.content = 'Приоритетность: ' + contentText
            return this.content
        }
    },
    descriptionCard: {
        tag: 'p',
        attr: {
            className: 'card-description'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Описание: ' + contentText;
            return this.content
        }
    }
}
export const stateDentist = {
    titleCard: {
        tag: 'h3',
        attr: {
            className: 'card-fio'
        },
        content: '',
        getContent(contentText){
            this.content = contentText
            return this.content
        }
    },
    doctorCard: {
        tag: 'span',
        attr: {
            className: 'card-doctor'
        },
        content: 'Врач: Стоматолог'
    },
    blockCard: {
        tag: 'div',
        attr: {
            className: 'card-block',
        }
    },
    purposeCard: {
        tag: 'span',
        attr: {
            className: 'card-purpose'
        },
        content: '',
        getContent(contentText) {
            this.content = 'Цель визита: ' + contentText
            return this.content
        }
    },
    lastDayCard: {
        tag: 'span',
        attr: {
            className: 'card-last-day'
        },
        content: '',
        getContent(contentText) {
            this.content = 'Дата последнего визита:' + contentText;
            return this.content
        }
    },
    priorityCard: {
        tag: 'span',
        attr: {
            className: 'card-priority'
        },
        content: '',
        getContent(contentText =  'Приоритетность: ') {
            this.content = 'Приоритетность: ' + contentText
            return this.content
        }
    },
    descriptionCard: {
        tag: 'p',
        attr: {
            className: 'card-description'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Описание: ' + contentText;
            return this.content
        }
    }
}
export const stateTherapist = {
    titleCard: {
        tag: 'h3',
        attr: {
            className: 'card-fio'
        },
        content: '',
        getContent: function(contentText) {
            this.content = contentText
            return this.content
        }
    },
    ageCard: {
        tag: 'span',
        attr: {
            className: 'card-age'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Возвраст: ' + contentText
            return this.content
        }
    },
    doctorCard: {
        tag: 'span',
        attr: {
            className: 'card-doctor'
        },
        content: 'Врач: Терапевт',
    },
    blockCard: {
        tag: 'div',
        attr: {
            className: 'card-block',
        }
    },
    purposeCard: {
        tag: 'span',
        attr: {
            className: 'card-purpose'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Цель визита: ' + contentText;
            return this.content
        }
    },
    priorityCard: {
        tag: 'span',
        attr: {
            className: 'card-priority'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Приоритетность: ' + contentText;
            return this.content
        }
    },
    descriptionCard: {
        tag: 'p',
        attr: {
            className: 'card-description'
        },
        content: '',
        getContent: function(contentText) {
            this.content = 'Описание: ' + contentText;
            return this.content
        }
    }
}
